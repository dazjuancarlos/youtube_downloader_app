import tkinter as tk
import customtkinter 
import os
from pytube import YouTube


def download(link):
    yt=YouTube(link)

    res = []

    for a in yt.streams:
        if str(a.type) == 'audio':
            res.append(a)

    max_a = res[0]

    for a in res:
        compare_max = int(str(max_a.abr).replace('kbps',''))
        compare_other = int(str(a.abr).replace('kbps',''))
        if compare_max < compare_other:
            max_a = a

    max_a.download('./downloads_youtube')

def check_download_folder():
    if os.path.isdir('downloads_youtube') == False:
        os.system('md downloads_youtube')
    return

def key_pressed(e):
    if e.keycode == 13:
        button_pressed()

def button_pressed():
    youtube_link = my_entry.get()#TODO check if there is a youtube url

    if youtube_link.find('https://youtu.be/') == 0:
        check_download_folder()
        download(youtube_link)
        my_entry.delete(0,len(youtube_link))
        os.system('explorer downloads_youtube')
    else:
        return

customtkinter.set_appearance_mode('dark') #Modes: system(defaut), light, dark
customtkinter.set_default_color_theme('blue') #Themes blue(default), dark-blue, green

root = customtkinter.CTk()  # create CTk window like you do with the Tk window
root.title('Youtube Downloader')
root.resizable(False,False)
root.geometry("620x100")

my_labelframe = customtkinter.CTkFrame(root, corner_radius=10)
my_labelframe.pack(pady=20)

#textbox
my_entry = customtkinter.CTkEntry(my_labelframe, width=400, height=30, border_width=1, placeholder_text='Youtube link video', text_color='silver')
my_entry.bind('<KeyPress>',command=key_pressed)
my_entry.grid(row=0, column=0,padx=10, pady=10)

#button
my_button = customtkinter.CTkButton(my_labelframe, text='Download', command=button_pressed)
my_button.grid(row=0, column=1,padx=10)

root.mainloop()



